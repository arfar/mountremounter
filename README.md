# Mount Remounter

_Very_ simple tool that checks if your Linux mounts are running, then runs a
command to try to remount it (you supply the command).

## Usage

Copy ``example_mount_points.py`` to ``mount_points.py`` and edit it. The
``mount_points.py`` file contains the ``mount_points`` list / tuple of tuples.
The innermost tuple comprises two elements: [0] mount folder, and [1] command
to run.

As a note, the command strings are NOT sanitised or checked or anything - they are
run as-is. Make sure you know what you're writing in there.

### Cron

I use this in cron so that my servers check every, for example, 5 minutes
everything is mounted properly etc.

``*/5 * * * * /usr/bin/python /home/<USER>/code/mountremounter/remount.py``
