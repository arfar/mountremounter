import sys
import subprocess

try:
    from mount_points import mount_points
except ImportError:
    print('Couldn\'t find the mount points. Please copy the example_mount_points.py')
    print('to mount_points.py and edit the file.')
    sys.exit(1)

def is_mounted(mount_point):
    lines = None
    with open('/proc/mounts', 'r') as f:
        lines = f.readlines()
    for line in lines:
        if mount_point in line:
            return True
    return False

if __name__ == '__main__':
    for mount_point in mount_points:
        if is_mounted(mount_point[0]):
            print('{} is mounted'.format(mount_point[0]))
        else:
            print('{} is not mounted; remounting using {}'.format(mount_point[0], mount_point[1]))
            subprocess.call(mount_point[1], shell=True)
            if is_mounted(mount_point[0]):
                print('{} is mounted again'.format(mount_point[0]))
            else:
                print('{} is not mounted - might need fixing'.format(mount_point[0]))

